# Slow control of Mesytec MRC-1 & MHV-4 modules

## Description :

Slow control on the high voltage for the silicon sensors. It use NIM modules from Mesytec : MRC-1 for remote control and MHV-4 for the HV power supply.

The application is written in python (version 3):
- mesytec_MRC.py : library to map the serial commands to be sent to MRC module via serial port

- pullDataHV.py : code for pull the data of the voltages, currents, on/off status of the MHV4 modules via the MRC-1. The data is saved into a sqlite database with the time of the pull saved. It will allow us to check the return current whenever the sensors deteriorate.

- HVviewer.py : web application based on Dash framework to present the most information from the sqlite database. It will also allow people to put on/off easily the high voltages if needed. The voltage values will not be set via this web interface but manually to make sure that nobody set wrong HV to the sensors without will.

- serverHV.py : start a wcgi server to localhost:8050 for the deployment of the web viewer as a production application

- SQLite_SiliconHV.db : The sqlite database of the HV data.

- pullDataHV.service & pullDataHV.timer : systemd service and timer to run the pullDataHV script every 10 minutes as system service

## How to start

* The following will start the web application of the viewer on localhost:8050 (localhost or 127.0.0.1):
```
python3 serverHV.py
```

* To start the pullDataHV service :
```
systemctl --user start pullDataHV.timer
```
This will start the service on a 10 minute cycle.
```
systemctl --user start pullDataHV.service
```
This will start on single instance of the pullDataHV service

```
systemctl --user status pullDataHV.timer pullDataHV.service
```
This will report the current status on the systemd management of the service

* To set the voltages : you will need to run a python prompt and import the library mesytec_MRC.py in which there are functions for setting the voltage for all the modules and all their channels. Please read the mesytec_MRC.py file and learn how to proceed or ask someone knowledgeable.


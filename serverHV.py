from waitress import serve
from HVviewer import server
import logging

logging.getLogger("waitress.queue").setLevel(logging.ERROR)

serve(server, host='127.0.0.1', port=8050, threads=1)

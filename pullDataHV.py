import sqlite3
from sqlite3 import Error

import mesytec_MRC as mrc
import datetime as DT

def pullModulesList(bus: int):
    sio = mrc.open_socket_serial()
    modules = mrc.scan_MRC(sio,bus)
    sio.close()
    return modules

def pullData(bus: int, modules: list):
    channels = [0,1,2,3]

    sio = mrc.open_socket_serial()
    mod_RC = {}
    for mod_id in modules:
        out = mrc.read_RC(sio, bus, mod_id)
        out1 = mrc.parse_response(out)
        out2 = mrc.post_parser(out1)
        while out2[0] != 1:
            print("!> retrying :")
            out = mrc.read_RC(sio, bus, mod_id)
            out1 = mrc.parse_response(out)
            out2 = mrc.post_parser(out1)

        mod_RC[mod_id]=out2[1]

    mod_onoff = {}
    for mod_id in modules:
        onoff = {}
        for ch_id in channels:
            out = mrc.read_OnOff(sio, bus, mod_id, ch_id)
            out1 = mrc.parse_response(out)
            out2_o = mrc.post_parser(out1)
            while out2_o[0] != 1:
                print("!> retrying :")
                out = mrc.read_OnOff(sio, bus, mod_id, ch_id)
                out1 = mrc.parse_response(out)
                out2_o = mrc.post_parser(out1)

            onoff[ch_id]= out2_o[1]

        mod_onoff[mod_id]= onoff

    mod_voltages = {}
    for mod_id in modules:
        volts = {}
        for ch_id in channels:
            out = mrc.read_Volt(sio, bus, mod_id, ch_id)
            out1 = mrc.parse_response(out)
            out2_v = mrc.post_parser(out1)
            while out2_v[0] != 1:
                print("!> retrying :")
                out = mrc.read_Volt(sio, bus, mod_id, ch_id)
                out1 = mrc.parse_response(out)
                out2_v = mrc.post_parser(out1)

            volts[ch_id]= out2_v[1]

        mod_voltages[mod_id]= volts

    mod_currents = {}
    for mod_id in modules:
        amps = {}
        for ch_id in channels:
            out = mrc.read_Current(sio, bus, mod_id, ch_id)
            out1 = mrc.parse_response(out)
            out2 = mrc.post_parser(out1)
            while out2[0] != 1:
                print("!> retrying :")
                out = mrc.read_Current(sio, bus, mod_id, ch_id)
                out1 = mrc.parse_response(out)
                out2 = mrc.post_parser(out1)

            amps[ch_id]= out2[1]

        mod_currents[mod_id]= amps

    datetime = DT.datetime.now()

    sio.close()

    return [datetime, modules,  mod_RC, mod_onoff, mod_voltages, mod_currents]

def pullDataTest():
    bus = 0
    modules = [0,1,5]
    channels = [0,1,2,3]

    mod_RC = {}
    for mod_id in modules:

        mod_RC[mod_id] = 1

    mod_onoff = {}
    for mod_id in modules:
        onoff = {}
        for ch_id in channels:
            onoff[ch_id]=1

        mod_onoff[mod_id]= onoff

    mod_voltages = {}
    for mod_id in modules:
        volts = {}
        for ch_id in channels:
            volts[ch_id]=1000*ch_id+mod_id

        mod_voltages[mod_id]= volts

    mod_currents = {}
    for mod_id in modules:
        amps = {}
        for ch_id in channels:
            amps[ch_id]= 100*ch_id+mod_id*2
        mod_currents[mod_id]=amps

    datetime = DT.datetime.now()

    return [datetime, modules,  mod_RC, mod_onoff, mod_voltages, mod_currents]


def createSQLite():
    sqliteConnection = sqlite3.connect('SQLite_SiliconHV.db')
    sqliteConnection.execute('''CREATE TABLE HVcontrol
                               (id INTEGER PRIMARY KEY,
                               datetime TEXT NOT NULL,
                               module0 INT NOT NULL,
                               module0_rc INT NOT NULL,
                               onoff0_ch0 INT NOT NULL,
                               onoff0_ch1 INT NOT NULL,
                               onoff0_ch2 INT NOT NULL,
                               onoff0_ch3 INT NOT NULL,
                               volt0_ch0 REAL,
                               volt0_ch1 REAL,
                               volt0_ch2 REAL,
                               volt0_ch3 REAL,
                               cur0_ch0 REAL,
                               cur0_ch1 REAL,
                               cur0_ch2 REAL,
                               cur0_ch3 REAL,
                               module1 INT NOT NULL,
                               module1_rc INT NOT NULL,
                               onoff1_ch0 INT NOT NULL,
                               onoff1_ch1 INT NOT NULL,
                               onoff1_ch2 INT NOT NULL,
                               onoff1_ch3 INT NOT NULL,
                               volt1_ch0 REAL,
                               volt1_ch1 REAL,
                               volt1_ch2 REAL,
                               volt1_ch3 REAL,
                               cur1_ch0 REAL,
                               cur1_ch1 REAL,
                               cur1_ch2 REAL,
                               cur1_ch3 REAL,
                               module2 INT NOT NULL,
                               module2_rc INT NOT NULL,
                               onoff2_ch0 INT NOT NULL,
                               onoff2_ch1 INT NOT NULL,
                               onoff2_ch2 INT NOT NULL,
                               onoff2_ch3 INT NOT NULL,
                               volt2_ch0 REAL,
                               volt2_ch1 REAL,
                               volt2_ch2 REAL,
                               volt2_ch3 REAL,
                               cur2_ch0 REAL,
                               cur2_ch1 REAL,
                               cur2_ch2 REAL,
                               cur2_ch3 REAL);''')

    print("Table HVcontrol created !")
    sqliteConnection.close()

def updateSQLite(bus: int, modules: list):
    try:
        sqliteConnection = sqlite3.connect('SQLite_SiliconHV.db',detect_types=sqlite3.PARSE_DECLTYPES)

        sql = ''' INSERT INTO HVcontrol(datetime, module0, module0_rc, onoff0_ch0, onoff0_ch1, onoff0_ch2, onoff0_ch3, volt0_ch0, volt0_ch1, volt0_ch2, volt0_ch3, cur0_ch0, cur0_ch1, cur0_ch2, cur0_ch3, module1, module1_rc,  onoff1_ch0, onoff1_ch1, onoff1_ch2, onoff1_ch3, volt1_ch0, volt1_ch1, volt1_ch2, volt1_ch3, cur1_ch0, cur1_ch1, cur1_ch2, cur1_ch3, module2, module2_rc, onoff2_ch0, onoff2_ch1, onoff2_ch2, onoff2_ch3, volt2_ch0, volt2_ch1, volt2_ch2, volt2_ch3, cur2_ch0, cur2_ch1, cur2_ch2, cur2_ch3)
              VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) '''

#        data = pullDataTest()
        data = pullData(bus, modules)
        print(data)
        modules = data[1]
        data_sql = (data[0], modules[0], data[2][modules[0]],
                    data[3][modules[0]][0], data[3][modules[0]][1], data[3][modules[0]][2], data[3][modules[0]][3],
                    data[4][modules[0]][0], data[4][modules[0]][1], data[4][modules[0]][2], data[4][modules[0]][3],
                    data[5][modules[0]][0], data[5][modules[0]][1], data[5][modules[0]][2], data[5][modules[0]][3],
                    modules[1], data[2][modules[1]],
                    data[3][modules[1]][0], data[3][modules[1]][1], data[3][modules[1]][2], data[3][modules[1]][3],
                    data[4][modules[1]][0], data[4][modules[1]][1], data[4][modules[1]][2], data[4][modules[1]][3],
                    data[5][modules[1]][0], data[5][modules[1]][1], data[5][modules[1]][2], data[5][modules[1]][3],
                    modules[2], data[2][modules[2]],
                    data[3][modules[2]][0], data[3][modules[2]][1], data[3][modules[2]][2], data[3][modules[2]][3],
                    data[4][modules[2]][0], data[4][modules[2]][1], data[4][modules[2]][2], data[4][modules[2]][3],
                    data[5][modules[2]][0], data[5][modules[2]][1], data[5][modules[2]][2], data[5][modules[2]][3])

        cur = sqliteConnection.cursor()
        cur.execute(sql,data_sql)

        sqliteConnection.commit()
        cur.close()
        sqliteConnection.close()

    except Error as e:
        print(e)


def readSQLite():
    try:
        sqliteConnection = sqlite3.connect('SQLite_SiliconHV.db')

        cur = sqliteConnection.cursor()

        cur.execute("SELECT * FROM HVcontrol WHERE datetime < ?",[DT.datetime.now()])
        print(cur.fetchall())

        sqliteConnection.commit()
        cur.close()
        sqliteConnection.close()

    except Error as e:
        print(e)


def updateSQLite_all():
    bus = 0
    modules = pullModulesList(bus)
    while len(modules) != 3:
        modules = pullModulesList(bus)

    updateSQLite(bus, modules)



if __name__ == '__main__':
    bus = 0
    modules = pullModulesList(bus)
    while len(modules) != 3:
        modules = pullModulesList(bus)

    updateSQLite(bus, modules)

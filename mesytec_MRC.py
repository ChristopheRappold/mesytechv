import serial
#import io
from time import sleep

profileMHV = {4 : {'index': 0,'name': 'channel0_enable_write',         'critical': True, 'default': 0},
              5 : {'index': 1,'name': 'channel1_enable_write',         'critical': True, 'default': 0},
              6 : {'index': 2,'name': 'channel2_enable_write',         'critical': True, 'default': 0},
              7 : {'index': 3,'name': 'channel3_enable_write',         'critical': True, 'default': 0},
              36: {'index': 0,'name': 'channel0_enable_read',          'read_only': True, 'poll': True},
              37: {'index': 1,'name': 'channel1_enable_read',          'read_only': True, 'poll': True},
              38: {'index': 2,'name': 'channel2_enable_read',          'read_only': True, 'poll': True},
              39: {'index': 3,'name': 'channel3_enable_read',          'read_only': True, 'poll': True},
              46: {'index': 0,'name': 'channel0_polarity_read',        'read_only': True, 'poll': True},
              47: {'index': 1,'name': 'channel1_polarity_read',        'read_only': True, 'poll': True},
              48: {'index': 2,'name': 'channel2_polarity_read',        'read_only': True, 'poll': True},
              49: {'index': 3,'name': 'channel3_polarity_read',        'read_only': True, 'poll': True},
              8 : {'index': 0,'name': 'channel0_current_limit_write',  'range': (0, 2000), 'units': "10nA", 'default': 2000},
              9 : {'index': 1,'name': 'channel1_current_limit_write',  'range': (0, 2000), 'units': "10nA", 'default': 2000},
              10: {'index': 2,'name': 'channel2_current_limit_write',  'range': (0, 2000), 'units': "10nA", 'default': 2000},
              11: {'index': 3,'name': 'channel3_current_limit_write',  'range': (0, 2000), 'units': "10nA", 'default': 2000},
              40: {'index': 0,'name': 'channel0_current_limit_read',   'read_only': True, 'poll': True, 'units': "10nA"},
              41: {'index': 1,'name': 'channel1_current_limit_read',   'read_only': True, 'poll': True, 'units': "10nA"},
              42: {'index': 2,'name': 'channel2_current_limit_read',   'read_only': True, 'poll': True, 'units': "10nA"},
              43: {'index': 3,'name': 'channel3_current_limit_read',   'read_only': True, 'poll': True, 'units': "10nA"},
              50: {'index': 0,'name': 'channel0_current_read',         'read_only': True, 'poll': True, 'units': "1.nA"},
              51: {'index': 1,'name': 'channel1_current_read',         'read_only': True, 'poll': True, 'units': "1.nA"},
              52: {'index': 2,'name': 'channel2_current_read',         'read_only': True, 'poll': True, 'units': "1.nA"},
              53: {'index': 3,'name': 'channel3_current_read',         'read_only': True, 'poll': True, 'units': "1.nA"},
              0 : {'index': 0,'name': 'channel0_voltage_write',        'range': (0, 8000), 'units': "0.1V", 'default': 0},
              1 : {'index': 1,'name': 'channel1_voltage_write',        'range': (0, 8000), 'units': "0.1V", 'default': 0},
              2 : {'index': 2,'name': 'channel2_voltage_write',        'range': (0, 8000), 'units': "0.1V", 'default': 0},
              3 : {'index': 3,'name': 'channel3_voltage_write',        'range': (0, 8000), 'units': "0.1V", 'default': 0},
              32: {'index': 0,'name': 'channel0_voltage_read',         'read_only': True, 'poll': True, 'units': "0.1V"},
              33: {'index': 1,'name': 'channel1_voltage_read',         'read_only': True, 'poll': True, 'units': "0.1V"},
              34: {'index': 2,'name': 'channel2_voltage_read',         'read_only': True, 'poll': True, 'units': "0.1V"},
              35: {'index': 3,'name': 'channel3_voltage_read',         'read_only': True, 'poll': True, 'units': "0.1V"},
              44: {'index':"",'name': 'rc_enable', 'read_only': True},
              13: {'index':"",'name': 'voltage_range_write', 'range': (0, 1), 'default': 400},
              45: {'index':"",'name': 'voltage_range_read', 'read_only': True, 'poll': True}}


def open_socket_serial():
    ser = serial.Serial('/dev/ttyUSB0',baudrate=9600,
                        bytesize=8, parity='N', stopbits=1,
                        timeout=1)
    ser.close()
    ser.open()
    print(ser.is_open)

    return ser
#   sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser, 1), newline=None)
#   sio._CHUNK_SIZE = 1
#    return sio


def readout_serial(ser):
    word_out = b""
    temp_w = ser.readline()
    while temp_w != b"\rmrc-1>":
#        print("debug>",temp_w)
        word_out += temp_w
#        sleep(0.05)
        temp_w = ser.readline()

    return word_out.decode()

def parse_response(word):
    word = word.strip()
    status = "Fine"
    if word.find("ERROR") != -1:
        status = "ERROR"
    if word.find("NO RESP") != -1:
        status = "NO RESP"

    if status != "Fine":
        return [status, {""}]

    list_w = word.split()
    if len(list_w) == 5:
        mrc_cmd = list_w[0]
        bus_id = list_w[1]
        module_id = list_w[2]
        module_cmd = list_w[3]
        value_cmd = list_w[4]

        return [status, {"mrc_cmd": mrc_cmd, "bus_id":bus_id, "module_id":module_id, "module_cmd":int(module_cmd), "value":value_cmd}]
    else:
        return ["ERR_PARSER",{""}]

def post_parser(list_w):
    if list_w[0] != "Fine":
        print("some error happened during operation -> retry:", list_w[0])
        return [-1, 0]
    else:
        info = profileMHV[list_w[1]["module_cmd"]]
        value = list_w[1]["value"]
        value2 = str(value)
        if "units" in profileMHV[list_w[1]["module_cmd"]]:
            if profileMHV[list_w[1]["module_cmd"]]["units"] == "10nA":
                value = round(float(value) * 10,1)
                value2 = str(value) + " nA"
            if profileMHV[list_w[1]["module_cmd"]]["units"] == "1.nA":
                value2 = str(value) + " nA"
            if profileMHV[list_w[1]["module_cmd"]]["units"] == "0.1V":
                value = round(float(value) * 0.1,1)
                value2 = str(value) + " V"

        print(" command:",list_w[1]["mrc_cmd"],"done on bus [",list_w[1]["bus_id"],"] to module [",list_w[1]["module_id"],"] > ",
              info["name"],info["index"]," value :",value2)
        print(" ----- ")
        return [1, value]



def scan_MRC(sio, idbus : int):
    words = 'SC '+str(idbus)+'\r'
    sio.write(words.encode())
    sio.flush()
    response = readout_serial(sio)
    print(response)

    modules = []
    list_mod = response.split('\n')
    #print(list_mod)
    for mod_id in list_mod:
        mods = mod_id.replace('\r', '')
        list_w = mods.split(":")
        #print("mod:",list_w)
        if len(list_w) == 1:
            continue
        if list_w[1].find('-') == -1 and len(list_w[1])>1:
            list_m = list_w[1].split(',')
            #print(" type :",int(list_m[0]))
            if int(list_m[0]) == 17 or int(list_m[0]) == 27:
                modules.append(int(list_w[0]))

    return modules

def set_RC(sio, idbus:int, idmod: int, onoff: int):
    if onoff == 1:
        cmd = 'ON '
    if onoff == 0:
        cmd = 'OFF '
    words = str(cmd)+str(idbus)+' '+str(idmod)+'\r'
#    print(words)
    sio.write(words.encode())
    sio.flush()
    response = readout_serial(sio)
    print(response)
    return response

def set_Volt(sio, idbus :int, idmod :int, idch: int, volt: int):
    if idch == 0:
        par = ' 0 '
    if idch == 1:
        par = ' 1 '
    if idch == 2:
        par = ' 2 '
    if idch == 3:
        par = ' 3 '
    
    words = 'SE '+str(idbus)+' '+str(idmod)+str(par)+str(volt)+'\r'
#    print(words)
    sio.write(words.encode())
    sio.flush()
    response = readout_serial(sio)
    print(response)
    return response

def set_Ch_OnOff(sio, idbus:int, idmod: int, idch: int, onoff: int):
    if idch == 0:
        par = ' 4 '
    if idch == 1:
        par = ' 5 '
    if idch == 2:
        par = ' 6 '
    if idch == 3:
        par = ' 7 '

    if onoff != 0 and onoff !=1:
        onoff == 0
    
    words = 'SE '+str(idbus)+' '+str(idmod)+str(par)+str(onoff)+'\r'
#   print(words)
    sio.write(words.encode())
    sio.flush()
    response = readout_serial(sio)
    print(response)
    return response


def set_RangeVolt_OldMod(sio, idbus:int, idmod:int, limit100_400:int):
    if limit100_400 == 100:
        par = ' 0'
    elif limit100_400 == 400:
        par = ' 1'
    else:
        print("E> set_RangeVolt : limit must 100 V or 400 V")
        return

    words = 'SE '+str(idbus)+' '+str(idmod)+' 13 '+par+'\r'
    print(words)
    sio.write(words.encode())
    sio.flush()
    response = readout_serial(sio)
    print(response)
    return response

def set_RangeVolt_NewMod(sio, idbus:int, idmod:int, idch:int, limit800:int):
    if idch == 0:
        par = ' 18 '
    if idch == 1:
        par = ' 19 '
    if idch == 2:
        par = ' 20 '
    if idch == 3:
        par = ' 21 '

    words = 'SE '+str(idbus)+' '+str(idmod)+par+str(limit800)+'\r'
    print(words)
    sio.write(words.encode())
    sio.flush()
    response = readout_serial(sio)
    print(response)
    return response

def set_Polarity_NewMod(sio, idbus:int, idmod:int, idch:int, plus_neg:int):
    if idch == 0:
        par = ' 14 '
    if idch == 1:
        par = ' 15 '
    if idch == 2:
        par = ' 16 '
    if idch == 3:
        par = ' 17 '

    if plus_neg == -1:
        plus_neg = 0


    words = 'SE '+str(idbus)+' '+str(idmod)+par+str(plus_neg)+'\r'
    print(words)
    sio.write(words.encode())
    sio.flush()
    response = readout_serial(sio)
    print(response)
    return response


def set_CurrentLim(sio, idbus :int, idmod :int, idch: int, amp: int):
    if idch == 0:
        par = ' 8 '
    if idch == 1:
        par = ' 9 '
    if idch == 2:
        par = ' 10 '
    if idch == 3:
        par = ' 11 '

    words = 'SE '+str(idbus)+' '+str(idmod)+str(par)+str(amp)+'\r'
#    print(words)
    sio.write(words.encode())
    sio.flush()
    response = readout_serial(sio)
    print(response)
    return response



def read_Volt(sio, idbus:int, idmod:int, idch:int):
    if idch == 0:
        par = ' 32 '
    if idch == 1:
        par = ' 33 '
    if idch == 2:
        par = ' 34 '
    if idch == 3:
        par = ' 35 '

    words = 'RE '+str(idbus)+' '+str(idmod)+par+'\r'
    sio.write(words.encode())
    sio.flush()

    response = readout_serial(sio)
    print(response)
    return response

def read_OnOff(sio, idbus:int, idmod:int, idch:int):
    if idch == 0:
        par = ' 36 '
    if idch == 1:
        par = ' 37 '
    if idch == 2:
        par = ' 38 '
    if idch == 3:
        par = ' 39 '

    words = 'RE '+str(idbus)+' '+str(idmod)+par+'\r'
    sio.write(words.encode())
    sio.flush()

    response = readout_serial(sio)
    print(response)
    return response

def read_RC(sio, idbus:int, idmod:int):
    words = 'RE '+str(idbus)+' '+str(idmod)+' 44\r'
    print(" - to send >",words)
    sio.write(words.encode())
    sio.flush()

    response = readout_serial(sio)
    print(response)
    return response

def read_Range_OldMod(sio, idbus:int, idmod:int):
    words = 'RE '+str(idbus)+' '+str(idmod)+' 45\r'
    sio.write(words.encode())
    sio.flush()

    response = readout_serial(sio)
    print(response)
    return response

def read_Range_NewMod(sio, idbus:int, idmod:int, idch:int):
    if idch == 0:
        par = ' 22 '
    if idch == 1:
        par = ' 23 '
    if idch == 2:
        par = ' 24 '
    if idch == 3:
        par = ' 25 '

    words = 'RE '+str(idbus)+' '+str(idmod)+par+'\r'
    sio.write(words.encode())
    sio.flush()

    response = readout_serial(sio)
    print(response)
    return response

def read_CurrentLim(sio, idbus:int, idmod:int, idch:int):
    if idch == 0:
        par = ' 40 '
    if idch == 1:
        par = ' 41 '
    if idch == 2:
        par = ' 42 '
    if idch == 3:
        par = ' 43 '

    words = 'RE '+str(idbus)+' '+str(idmod)+par+'\r'
    sio.write(words.encode())
    sio.flush()

    response = readout_serial(sio)
    print(response)
    return response

def read_Current(sio, idbus:int, idmod:int, idch:int):
    if idch == 0:
        par = ' 50 '
    if idch == 1:
        par = ' 51 '
    if idch == 2:
        par = ' 52 '
    if idch == 3:
        par = ' 53 '

    words = 'RE '+str(idbus)+' '+str(idmod)+par+'\r'
    sio.write(words.encode())
    sio.flush()

    response = readout_serial(sio)
    print(response)
    return response

def set_onoff_all(bus:int, modules:int, onoff:int):

    channels = [0,1,2,3]
    sio = open_socket_serial()

    for mod_id in modules:
        for ch_id in channels:
            out = set_Ch_OnOff(sio, bus, mod_id, ch_id, onoff)
            out1 = parse_response(out)
            out2_o = post_parser(out1)
            while out2_o[0] != 1:
                print("!> retrying :")
                out = set_Ch_OnOff(sio, bus, mod_id, ch_id, onoff)
                out1 = parse_response(out)
                out2_o = post_parser(out1)


def set_On_all():
    bus = 0
    sio = open_socket_serial()
    modules = scan_MRC(sio,bus)
    sio.close()
    while len(modules) != 3:
        modules = scan_MRC(sio,bus)

    set_onoff_all(bus, modules, 1)

def set_Off_all():
    bus = 0
    sio = open_socket_serial()
    modules = scan_MRC(sio,bus)
    sio.close()
    while len(modules) != 3:
        modules = scan_MRC(sio,bus)

    set_onoff_all(bus, modules, 0)


def set_voltage_all(bus:int, table_volt: dict):

    sio = open_socket_serial()

    for mod_id in table_volt:
        for ch_id in table_volt[mod_id]:
            out = set_Volt(sio, bus, mod_id, ch_id, table_volt[mod_id][ch_id])
            out1 = parse_response(out)
            out2_v = post_parser(out1)
            while out2_v[0] != 1:
                print("!> retrying :")
                out = set_Volt(sio, bus, mod_id, ch_id, table_volt[mod_id][ch_id])
                out1 = parse_response(out)
                out2_v = post_parser(out1)


def set_current_all(bus:int, table_curlimit: dict):

    sio = open_socket_serial()

    for mod_id in table_curlimit:
        for ch_id in table_curlimit[mod_id]:
            out = set_CurrentLim(sio, bus, mod_id, ch_id, table_volt[mod_id][ch_id])
            out1 = parse_response(out)
            out2_v = post_parser(out1)
            while out2_v[0] != 1:
                print("!> retrying :")
                out = set_CurrentLim(sio, bus, mod_id, ch_id, table_volt[mod_id][ch_id])
                out1 = parse_response(out)
                out2_v = post_parser(out1)

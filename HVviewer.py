import dash
from dash import dcc
from dash  import html
import dash_bootstrap_components as dbc
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from dash.dependencies import Input, Output
import datetime as DT
import sqlite3

import pullDataHV
import mesytec_MRC

db = "/home/wasa/Workspace/mesytechv/SQLite_SiliconHV.db"

app = dash.Dash(external_stylesheets=[dbc.themes.LUX])
server = app.server
app.config['suppress_callback_exceptions']=True

mods = ["0","1","2"]
channels = ["0","1","2","3"]
g_index = 1

app.layout = dbc.Container(
    [
     dcc.Store(id="store"),
     html.Div([
         html.H3('Silicon Power supply Mesytec MRC-1 / MHV-4'),
         html.Hr(),
#         row,
        html.Div(id='live-update-text'),
         html.Hr(),

#        dcc.Graph(id='live-update-graph'),
         html.Div( [
                     dbc.Row( html.H5('Settings')),
                     dbc.Row( [dbc.Col([dcc.ConfirmDialogProvider(
                         children=dbc.Button('ON ALL HV', color="primary"),
                         id='danger-set-on',
                         message='Danger going to put ON HV ! Are you sure you want to continue?'
                         ),
                        html.Div(id="set-on")]),
                               dbc.Col([dcc.ConfirmDialogProvider(
                         children=dbc.Button('OFF ALL HV', color="primary"),
                         id='danger-set-off',
                         message='Danger going to put OFF HV ! Are you sure you want to continue?'
                         ),
                        html.Div(id="set-off")]),
                               dbc.Col([dcc.ConfirmDialogProvider(
                         children=dbc.Button('Manual Update', color="primary"),
                         id='danger-manual-update',
                         message='Going to update DB manually ! It is not needed, are you sure you want to continue?'
                         ),
                        html.Div(id="set-update")])

                               ])
                ]),


         html.Hr(),
         html.Div(id='live-update-voltage'),
         html.Hr(),

         html.Div(id='live-update-current'),
        dcc.Interval(
            id='interval-component',
            interval=1*10000, # in milliseconds
            n_intervals=10000
            )
         ]
         )
    ]
    )



# @app.callback(
#     Output('clientside-figure-store-px', 'data'),
#     Input('clientside-graph-indicator-px', 'value'),
#     Input('clientside-graph-country-px', 'value')
# )
# def update_store_data(indicator, country):
#     dff = df[df['country'] == country]
#     return px.scatter(dff, x='year', y=str(indicator))

@app.callback(Output("store","data"),
              Input('interval-component', 'n_intervals'))
def update_database(n):
     conn = sqlite3.connect(db)
     data_limit20 = pd.read_sql("SELECT * FROM (SELECT * FROM HVcontrol ORDER BY datetime DESC LIMIT 20) ORDER BY datetime ASC",conn)#,parse_dates='datetime')

#     print(data_limit20)

     return {'datalimit':data_limit20.to_dict()}


@app.callback(Output("set-on", "children"),
              Input('danger-set-on', 'submit_n_clicks'))
def update_setOn(submit_n_clicks):
    if not submit_n_clicks:
        return ''
    else:
        print("function set On HV detectors")
        mesytec_MRC.set_On_all()
        return """ Set On HV ! Submitted {} times """.format(submit_n_clicks)

@app.callback(Output("set-off", "children"),
              Input('danger-set-off', 'submit_n_clicks'))
def update_setOff(submit_n_clicks):
    if not submit_n_clicks:
        return ''
    else:
        print("function set Off HV detectors")
        mesytec_MRC.set_Off_all()
        return """ Set Off HV ! Submitted {} times """.format(submit_n_clicks)

@app.callback(Output("set-update", "children"),
              Input('danger-manual-update', 'submit_n_clicks'))
def update_updateDB(submit_n_clicks):
    if not submit_n_clicks:
        return ''
    else:
        print("function update DB HV detectors")
        pullDataHV.updateSQLite_all()
        return """ Update DB HV ! Submitted {} times """.format(submit_n_clicks)


@app.callback(Output('live-update-text', 'children'),
#              [Input('interval-component','n_intervals'), Input("store","datalimit")]
              Input("store","data")
              )
def update_metrics(dataL):

    datalimit20 = dataL["datalimit"]
    list_key = [ int(key) for key in datalimit20['id'].keys()]
    index = str(sorted(list_key)[-1])
#    print("index", )
    strOnOff = "onoff"
    strV = "volt"
    strC = "cur"
    mods = ["0","1","2"]
    strCh = "_ch"
    channels = ["0","1","2","3"]
#    .strftime("%X %d %b %Y")
    html_list = [html.H4(" Last Update : "+ DT.datetime.fromisoformat(datalimit20["datetime"][index]).strftime("%X | %d %b %Y"))]
    html_volt = []
    html_cur = []
    for mod in mods:
        for ch in channels:
            name_cl = strOnOff+mod+strCh+ch
            if datalimit20[name_cl][index] == 1:
                html_list.append( html.P("Module "+mod+" Channel "+ch+' ON', style={"color":'green'}) )
            else:
                html_list.append( html.P("Module "+mod+" Channel "+ch +' OFF', style={"color":"red"}) )

            name_v = strV+mod+strCh+ch
            html_volt.append(html.P(str(datalimit20[name_v][index])+" V"))
            name_c = strC+mod+strCh+ch
            html_cur.append(html.P(str(datalimit20[name_c][index])+" nA"))

    return   [
        dbc.Row( html_list[0] ),
        dbc.Row([dbc.Col(html_span)  for html_span in html_list[1:]] ),
        dbc.Row([dbc.Col(html_span)  for html_span in html_volt] ),
        dbc.Row([dbc.Col(html_span)  for html_span in html_cur] )

        ]


@app.callback(Output('live-update-voltage', 'children'),
#              [Input('interval-component','n_intervals'), Input("store","datalimit")]
              Input("store","data")
              )
def update_voltage(dataL):

    datalimit20 = pd.DataFrame.from_dict(dataL["datalimit"])
    #datalimit20['datetime']=pd.to_datetime(datalimit20['datetime'])
    #print(datalimit20['datetime'])
    strV = "volt"
    mods = ["0","1","2"]
    strCh = "_ch"
    channels = ["0","1","2","3"]

    rows = [dbc.Row( html.H5("Voltage"))]
    for mod in mods:

        graph_volt = []
        for ch in channels:
            name_cl = strV+mod+strCh+ch
            fig = go.Figure(go.Scatter(x=datalimit20['datetime'],y=datalimit20[name_cl], mode='lines+markers'))
            fig.update_layout(title = "Module "+mod+" Channel "+ch, yaxis_title="Voltage (V)", margin_l = 5, margin_r=5, margin_b = 20, margin_t = 90)

            graph_volt.append(dbc.Col(dcc.Graph(figure=fig
#                px.line(datalimit20, x='datetime', y=name_cl, title="Voltage "+mod+" Channel "+ch, markers=True)
                                                    ) ))

        rows.append(dbc.Row([dbc.Col(graph)  for graph in graph_volt] ))

    return   [row for row in rows]


@app.callback(Output('live-update-current', 'children'),
#              [Input('interval-component','n_intervals'), Input("store","datalimit")]
              Input("store","data")
              )
def update_current(dataL):

    datalimit20 = pd.DataFrame.from_dict(dataL["datalimit"])

    strC = "cur"
    mods = ["0","1","2"]
    strCh = "_ch"
    channels = ["0","1","2","3"]

    rows = [dbc.Row( html.H5("Current"))]
    for mod in mods:
        graph_current = []
        for ch in channels:
            name_cl = strC+mod+strCh+ch
            fig = go.Figure(go.Scatter(x=datalimit20['datetime'],y=datalimit20[name_cl], mode='lines+markers'))
            fig.update_layout(title = "Module "+mod+" Channel "+ch, yaxis_title="Current (nA)", margin_l = 5, margin_r=5,margin_b = 20, margin_t = 90)
            graph_current.append(dbc.Col(dcc.Graph(figure=fig
#                px.line(datalimit20, x='datetime', y=name_cl, title="Current "+mod+" Channel "+ch)
                )))

        rows.append(dbc.Row([dbc.Col(graph)  for graph in graph_current] ))

    return   [row for row in rows]



if __name__ == '__main__':
#    app.run_server(debug=True)
    app.run_server(debug=False)
